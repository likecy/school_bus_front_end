import Vue from 'vue'
import App from './App'


App.mpType = 'app'
import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)

Vue.config.productionTip = false

const app = new Vue({
    ...App
})
app.$mount()
